// test.js

const puppeteer = require('puppeteer');
const assert = require('assert');

const extensionPath = '../../../../../../chrome';
let popupPage = null;
let optionsPage = null;
let contentPage = null;
let browser = null;

describe('Extension UI Testing', function () {
    this.timeout(20000); // default is 2 seconds and that may not be enough to boot browsers and pages.
    before(async function () {
        await boot();
    });

    //#region Util Tests
    describe('Utils Tests', async function () {
        it('Clean URL Tests', async function () {

            const test1 = await popupPage.evaluate(() => {
                return utils.cleanUrl(new URL("https://www.t.co/0/"));
            });
            assert.equal(test1, "https://www.twitter.com/0");

            const result = await popupPage.evaluate(() => {
                var links = [{ text: "a", url: "https://t.co/0/" }, { text: "Twitter", url: "https://twitter.com/0" }, { text: "Twitter", url: "https://twitter.com/0/" }];
                return utils.deDupLinkArray(links);
            });
            assert.equal(Object.keys(result)[0], "https://twitter.com/0");
            assert.equal(Object.keys(result).length, 1, "Should only be one link");

        });
    });

    //#endregion Util Tests

    //#region Popup UI Tests

    describe('Popup UI', async function () {
        it('Auto Highlight Disabled On Launch', async function () {
            await popupPage.evaluate(() => {
                UserSettings.fetch().then((settings) => {
                    settings.autoHighlight = false; settings.commit();
                    resetPopupUI()
                });
            });

            await checkPopupAutoHighlightEnabled(false);
            await checkMessageBarNotRendered();

            await popupPage.$eval('#autoHighlight', element => element.click());
            await checkMessageBarNotRendered();

            await popupPage.evaluate(() => {
                UserSettings.fetch().then((settings) => { settings.autoHighlight = true; settings.commit(); });
            });
        })
        it('Auto Highlight Disabled After Launch', async function () {
            await popupPage.evaluate(() => {
                UserSettings.fetch().then((settings) => {
                    settings.autoHighlight = true; settings.commit();
                    resetPopupUI()
                });
            });

            await checkPopupAutoHighlightEnabled(true);
            await checkMessageBarNotRendered();

            await popupPage.$eval('#autoHighlight', element => element.click());
            await checkMessageBarRendered("Auto-Highlight has been disabled");

            await popupPage.$eval('#autoHighlight', element => element.click());
            await checkMessageBarNotRendered();
        })
        it('Privacy Message on First Run', async function (){
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org/"
                    },
                    totalLinkCount: 7,
                    externalLinks: [{ text: "Pittsburgh", url: "https://pittsburgh.cbslocal.com/" }, { text: "Twitter", url: "https://twitter.com/0" }, { text: "Twitter", url: "https://twitter.com/1" }]
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered();
            await checkMessageBarRendered("Please review our Privacy Settings\n(Available in the Options menu)");
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });

            // Unset the Privacy setting so it's not garbling up the later tests
            await popupPage.evaluate(() => {
                UserSettings.fetch()
                    .then(settings => {settings.lastPrivacyUpdateVersion = constants.version; settings.commit();})
            });

        })
        it('Pie Chart', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org/"
                    },
                    totalLinkCount: 7,
                    externalLinks: [{ text: "Pittsburgh", url: "https://pittsburgh.cbslocal.com/" }, { text: "Twitter", url: "https://twitter.com/0" }, { text: "Twitter", url: "https://twitter.com/1" }]
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered();
            await checkMessageBarNotRendered();
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });

            await popupPage.$eval('#autoHighlight', element => element.click());
            await checkMessageBarRendered("Auto-Highlight has been disabled");

            await popupPage.$eval('#autoHighlight', element => element.click());
            await checkMessageBarNotRendered();
        })
        it('Pie Chart - New', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org"
                    },
                    totalLinkCount: 7,
                    externalLinks: [{ text: "Pittsburgh", url: "https://pittsburgh.cbslocal.com/" }, { text: "Twitter", url: "https://twitter.com/0" }, { text: "Twitter", url: "https://twitter.com/1" }]
                }

                handlePostMessages({ message: constants.postMessages.NewArticleFound, text: articleData.externalLinks.length });
                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered();
            await checkMessageBarRendered("You found a new article!");
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });
            await popupPage.evaluate(() => { newArticle = false });
        })
        it('Pie Chart - Force Scanned', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org"
                    },
                    totalLinkCount: 7,
                    externalLinks: [{ text: "Pittsburgh", url: "https://pittsburgh.cbslocal.com/" }, { text: "Twitter", url: "https://twitter.com/0" }, { text: "Twitter", url: "https://twitter.com/1" }]
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: true });
            });

            await checkChartsRendered();
            await checkMessageBarRendered("This site is not yet supported\nResults may be inaccurate");
            await checkButtonsRendered({ text: "Find Article", shown: true }, { text: "Ignore npr.org", shown: true });
        })
        it('Bar Chart', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org"
                    },
                    totalLinkCount: 10,
                    externalLinks: [{ text: "", url: "https://twitter.com" }, { text: "", url: "https://t.co/1" }, { text: "", url: "https://t.co/1" }, { text: "", url: "https://b.com/0" }, { text: "", url: "https://c.com/1" }, { text: "", url: "https://d.com/1" }, { text: "", url: "https://e.com/2" }, { text: "", url: "https://f.com/1" }, { text: "", url: "https://g.com/2" }]
                }
                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered();
            await checkMessageBarNotRendered();
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });
        })
        it('No Links', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org/"
                    },
                    totalLinkCount: 0,
                    externalLinks: []
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered(false, false);
            await checkMessageBarRendered("There are no links in this article.");
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });
        })
        it('No Links - New Article', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org/"
                    },
                    totalLinkCount: 0,
                    externalLinks: []
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
                handlePostMessages({ message: constants.postMessages.NewArticleFound});
            });

            await checkChartsRendered(false, false);
            await checkMessageBarRendered("There are no links in this article.");
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });
        })
        it('No External Links', async function () {
            await popupPage.evaluate(() => {
                articleData = {
                    article: {
                        title: "",
                        date: "",
                        location: "https://www.npr.org/"
                    },
                    totalLinkCount: 4,
                    externalLinks: []
                }

                handlePostMessages({ message: constants.postMessages.LinkAnalysisResult, result: articleData, unsupported: false });
            });

            await checkChartsRendered(true, false);
            await checkMessageBarRendered("There are no external links in this article.");
            await checkButtonsRendered({ text: "Rescan Page", shown: true }, { shown: false });
        })
        it('Unsupported Domain', async function () {
            await popupPage.evaluate(() => {
                handlePostMessages({ message: constants.postMessages.SiteNotSupported, hostname: "foo.bar" });
            });

            await checkMessageBarRendered("This site is not yet supported\nResults may be inaccurate");
            await checkButtonsRendered({ text: "Find Article", shown: true }, { text: "Ignore foo.bar", shown: true });
        })
        it('Ignored Domain', async function () {
            await popupPage.evaluate(() => {
                UserSettings.fetch()
                    .then(settings => settings.addIgnoredUrl("foo.bar"))
                showErrorMessage("foo.bar");
                handlePostMessages({ message: constants.postMessages.SiteNotSupported, hostname: "foo.bar" });
            });

            await checkMessageBarRendered("You've marked this as a site to ignore.");
            await checkButtonsRendered({ text: "Stop Ignoring foo.bar", shown: true }, { shown: false });

            await popupPage.evaluate(() => {
                UserSettings.fetch()
                    .then(settings => settings.removeIgnoredUrl("foo.bar"))
            });
        })
        it('Article not Found', async function () {
            await popupPage.evaluate(() => {
                handlePostMessages({ message: constants.postMessages.ArticleNotFound });
            });

            await checkMessageBarRendered("No Article Found");
            await checkButtonsRendered({ text: "Find Article", shown: true }, { shown: false });
        })
        it('Open Options Page', async function () {
            const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));
            await popupPage.$eval('#options', element => element.firstElementChild.click());
            optionsPage = await newPagePromise;
            await optionsPage.bringToFront();
            assert.equal(await optionsPage.title(), "Primary Source Scraper Options");
        })
    });
    //#endregion Popup UI Tests

    //#region Options UI Tests

    describe('Options UI', async function () {
        it('Auto Highlight Enabled On Launch', async function () {
            await checkOptionsAutoHighlightEnabled();
        });
        it('Auto Highlight Changes Settings', async function () {
            var currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.autoHighlight; });
            });
            assert.ok(currentSetting, "Auto Highlight Enabled")

            await optionsPage.$eval('#autoHighlight', element => element.click());
            await checkOptionsAutoHighlightEnabled(false);

            currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.autoHighlight; });
            });
            assert.ok(!currentSetting, "Auto Highlight Disabled")
        });
        it('Settings Changes Auto Highlight', async function () {
            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { settings.autoHighlight = true; settings.commit() });
            });

            await optionsPage.reload();
            await optionsPage.waitForTimeout(500);
            await checkOptionsAutoHighlightEnabled();
        });
        it('Send Article Data Disabled On Launch', async function () {
            await checkSendArticleDataEnabled(false);
        });
        it('Send Article Data Changes Settings', async function () {
            var currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.sendArticleData; });
            });
            assert.ok(!currentSetting, "Send Data Enabled")

            await optionsPage.$eval('#sendArticleData', element => element.click());
            await checkSendArticleDataEnabled();

            currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.sendArticleData; });
            });
            assert.ok(currentSetting, "Send Data Disabled")
        });
        it('Settings Changes Send Article Data', async function () {
            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { settings.sendArticleData = false; settings.commit() });
            });

            await optionsPage.reload();
            await checkSendArticleDataEnabled(false);
        });
        it('Send Error Reports Disabled On Launch', async function () {
            await checkSendErrorReportsEnabled(false);
        });
        it('Send Error Reports Changes Settings', async function () {
            var currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.sendErrorReports; });
            });
            assert.ok(!currentSetting, "Send Error Reports Enabled")

            await optionsPage.$eval('#sendErrorReports', element => element.click());
            await checkSendErrorReportsEnabled();

            currentSetting = await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { return settings.sendErrorReports; });
            });
            assert.ok(currentSetting, "Send Error Reports Disabled")
        });
        it('Settings Changes Send Error Reports', async function () {
            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then((settings) => { settings.sendErrorReports = false; settings.commit() });
            });

            await optionsPage.reload();
            await checkSendErrorReportsEnabled(false);
        });
        it('Supported Domains Loaded', async function () {
            var supportedDomainsListRows = await optionsPage.$eval('#supportedDomainsList', element => element.childElementCount);
            var supportedDomainsCount = parseInt(await optionsPage.$eval('#supportedDomainCount', element => element.innerText));

            assert.ok(supportedDomainsListRows > 0, "Supported Domains Loaded");
            assert.equal(supportedDomainsCount, supportedDomainsListRows, "UI Matches");
        });
        it('Ignored Domains Loaded', async function () {
            // Reset to Default
            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then(settings => settings.resetIgnoredUrlsToDefault());
            });
            await optionsPage.reload();
            await optionsPage.$eval('#ignoredDomainsTabButton', element => element.click());
            await checkIgnoredListRendered(15);
        });
        it('Ignored Domains Clear', async function () {

            await optionsPage.$eval('#removeAll', element => element.click());
            await checkIgnoredListRendered(0);
        });
        it('Ignored Domains Add Defaults', async function () {

            await optionsPage.$eval('#resetToDefault', element => element.click());
            await checkIgnoredListRendered(15);
        });
        it('Ignored Domains Add', async function () {

            var ignoredDomainsCount = parseInt(await optionsPage.$eval('#ignoredDomainCount', element => element.innerText));
            await optionsPage.type('#ignoredDomainsSearch', 'foo.com', { delay: 20 })

            await optionsPage.$eval('#add', element => element.click());

            await checkIgnoredListRendered(ignoredDomainsCount + 1);
        });
        it('Ignored Domains Remove', async function () {

            var ignoredDomainsCount = parseInt(await optionsPage.$eval('#ignoredDomainCount', element => element.innerText));
            await optionsPage.type('#ignoredDomainsSearch', 'foo.com', { delay: 20 })

            await optionsPage.$eval('#remove', element => element.click());

            await checkIgnoredListRendered(ignoredDomainsCount - 1);
        });

    });

    //#endregion Options UI Tests

    //#region Content Tests

    describe('Content Script', async function () {
        it('Auto Highlight Enabled On Launch', async function () {
            contentPage = await browser.newPage();
            await contentPage.goto('https://www.npr.org/2020/06/16/877601170/watch-live-trump-to-sign-executive-order-on-police-reform');

            await contentPage.waitForTimeout(3000);
            await checkLinkHighlighted("a[href^='https://pittsburgh.cbslocal.com']");
            await checkEmbedHighlighted("[id^='twitter-widget']");
        });
        it('Internal Link Not Highlighted', async function () {
            await checkLinkHighlighted("a[href^='https://www.npr.org/2020/06/11/875056309/congress-heads-toward-clash']", false);
        });
        it('Auto Highlight Disabled On Launch', async function () {
            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then(settings => { settings.autoHighlight = false; settings.commit() });
            });

            await contentPage.reload();
            await contentPage.waitForTimeout(3000);
            await checkLinkHighlighted("a[href^='https://pittsburgh.cbslocal.com']", false);
            await checkEmbedHighlighted("[id^='twitter-widget']", false)
        });
        it('Link Color + Contrast', async function () {

            await optionsPage.evaluate(() => {
                return UserSettings.fetch()
                    .then(settings => { settings.highlighterColor = "#8c564b"; settings.autoHighlight = true; settings.commit() });
            });

            await contentPage.reload();
            await contentPage.waitForTimeout(3000);
            await checkLinkHighlighted("a[href^='https://pittsburgh.cbslocal.com']", true, "rgb(140, 86, 75)", "rgb(255, 255, 255)");
            await checkEmbedHighlighted("[id^='twitter-widget']", true, "rgb(140, 86, 75)");
        });

    });

    //#endregion Content Tests

    after(async function () {
        await browser.close();
    });
});

async function boot() {
    browser = await puppeteer.launch({
        headless: false, // extension are allowed only in head-full mode
        args: [
            `--disable-extensions-except=${extensionPath}`,
            `--load-extension=${extensionPath}`
        ]
    });

    const targets = await browser.targets();
    const extensionTarget = targets.find(({ _targetInfo }) => {
        return _targetInfo.title === 'Primary Source Scraper';
    });

    const extensionUrl = extensionTarget._targetInfo.url || '';
    const [, , extensionID] = extensionUrl.split('/');
    const extensionPopupHtml = 'html/popup.html'
    const extensionOptionsHtml = 'html/options.html'

    popupPage = await browser.newPage();
    await popupPage.goto(`chrome-extension://${extensionID}/${extensionPopupHtml}`);
}

//#region Test Helpers
async function checkChartsRendered(ratio = true, details = true) {
    const ratioChart = await popupPage.$eval('#externalLinkRatio', element => element.style.display);
    assert.equal(ratio ? "block" : "none", ratioChart, 'Ratio Chart Failed to render');

    const externalChart = await popupPage.$eval('#externalLinkDetails', element => element.style.display);
    assert.equal(details ? "block" : "none", externalChart, 'Detail Chart Failed to render');
}

async function checkButtonsRendered(left = { text: "", shown: true }, right = { text: "", shown: false }) {

    const leftButtonStyle = await popupPage.$eval('#leftButton', element => element.style.display);
    assert.equal(left.shown ? "" : "none", leftButtonStyle, 'Left Button Display State');

    const rightButtonStyle = await popupPage.$eval('#rightButton', element => element.style.display);
    assert.equal(right.shown ? "" : "none", rightButtonStyle, 'Right Button Text');

    if (left.shown)
    {
        const leftButtonText = await popupPage.$eval('#leftButton', element => element.innerText);
        assert.equal(left.text, leftButtonText, 'Left Button Text');
    }

    if (right.shown)
    {
        const rightButtonText = await popupPage.$eval('#rightButton', element => element.innerText);
        assert.equal(right.text, rightButtonText, 'Right Button Display Text');
    }
}

async function checkMessageBarRendered(message) {
    var newMessageBarStyle = await popupPage.$eval('#messageBar', element => element.style.display);
    assert.ok(newMessageBarStyle == "", 'Message Bar not rendered');

    const messageBarText = await popupPage.$eval('#messageBarMessage', element => element.innerText);
    assert.equal(message, messageBarText, 'Message Bar Text is wrong');
}

async function checkMessageBarNotRendered() {
    var newMessageBarStyle = await popupPage.$eval('#messageBar', element => element.style.display);
    assert.ok(newMessageBarStyle == "none", 'Message Bar rendered');
}

async function checkPopupAutoHighlightEnabled(enabled = true) {
    var checked = await popupPage.$eval('#autoHighlight', element => !element.checked);
    assert.equal(checked, enabled, 'Auto Highlight Checkbox');
}

async function checkOptionsAutoHighlightEnabled(enabled = true) {
    var checked = await optionsPage.$eval('#autoHighlight', element => element.checked);
    assert.equal(checked, enabled, 'Auto Highlight Checkbox');
}

async function checkSendArticleDataEnabled(enabled = true) {
    var checked = await optionsPage.$eval('#sendArticleData', element => element.checked);
    assert.equal(checked, enabled, 'Send Article Data Checkbox');
}

async function checkSendErrorReportsEnabled(enabled = true) {
    var checked = await optionsPage.$eval('#sendErrorReports', element => element.checked);
    assert.equal(checked, enabled, 'Send Error Reports Checkbox');
}

async function checkIgnoredListRendered(expectedCount) {
    var ignoredDomainsListRows = await optionsPage.$eval('#ignoredDomainsList', element => element.childElementCount);
    assert.equal(ignoredDomainsListRows, expectedCount == 0 ? 1 : expectedCount, "Ignored Domains Count");
    ignoredDomainsCount = parseInt(await optionsPage.$eval('#ignoredDomainCount', element => element.innerText));
    assert.equal(ignoredDomainsCount, expectedCount, "UI Matches");
}

async function checkLinkHighlighted(querySelector, highlighted = true, backgroundColor = "rgb(0, 191, 255)", textColor = "rgb(0, 0, 0)") {
    var linkBackgroundColor = await contentPage.$eval(querySelector, element => element.style.backgroundColor);
    if (highlighted)
    {
        assert.equal(linkBackgroundColor, backgroundColor, "Link highlighted");

        var linkColor = await contentPage.$eval(querySelector, element => element.style.color);
        assert.equal(linkColor, textColor, "Link text contrast");
    }
    else
    {
        assert.notEqual(linkBackgroundColor, backgroundColor, "Link not highlighted");
    }
}

async function checkEmbedHighlighted(querySelector, highlighted = true, backgroundColor = "rgb(0, 191, 255)") {
    var expectedStyle = "5px solid " + backgroundColor;
    var embedRendered = await contentPage.$eval(querySelector, element => element != undefined);
    if (embedRendered)
    {
        var linkBackgroundColor = await contentPage.$eval(querySelector, element => element.style.border);
        if (highlighted)
        {
            assert.equal(linkBackgroundColor, expectedStyle, "Link highlighted");
        }
        else
        {
            assert.notEqual(linkBackgroundColor, expectedStyle, "Link not highlighted");
        }
    }
}
//#endregion Test Helpers