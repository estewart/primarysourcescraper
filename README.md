# [Primary Source Scraper](https://primarysourcescraper.com/)

A browser extension designed to highlight links to primary sources in news articles.

News lives on the internet now!  Various news sources are taking advantage of that by linking to sources, but it's not always clear that they're linking to **external** sources.  It's even less clear whether they're linking to **trustworthy** external sources.

This project has a few goals:
1. Make it clear whether a news article links to any external sources that readers can use to independently verify that the news is being reported accurately.
2. Highlight the links to those sources
3. (Eventually) Point out the relative trustworthiness of those sources (e.g. a **.gov** url is likely more trustworthy of a source than a tweet)

## Installation
You can download the latest published versions from [the website](https://primarysourcescraper.com/tabs/download/).

## Development

The bulk of this project is currently written in plain old Javascript, but our tests are dependent on [Mocha](https://mochajs.org/) and [Puppeteer](https://developers.google.com/web/tools/puppeteer/).  

There's one dependency on [chart.js](https://www.chartjs.org/), which has been copied to this repo.

### Code (under chrome/js/)
Almost all of the work in this project is handled by **content.js** and **popup.js**.
#### content.js
Does the work of actually parsing the DOM looking for external links.  It is heavily dependent on QuerySelectors.

#### popup.js
Displays some charts showing statistics about the links on a given page.

#### UserSettings.js, options.js
These handle dealing with User Settings, and helping the user actually set those settings via the Options page

#### ArticleData.js
Handles all of the data that can get sent over the wire as part of Article Data.  **Changes to this file require changes to the Privacy Policy**

### Tests (under tests/)
Our test suite consists of a bunch of Puppeteer tests running in head-ful mode.  Running these tests requires [npm](https://www.npmjs.com/get-npm).

On initial setup, go to the tests folder and run `npm install`.

For subsequent test runs, you can run `npm test`.  

### Configs (under chrome/settings/)
#### newsSources.json
Every news source is different, but they all basically have a section of the DOM that contains the news article, and that links to other things.  For each news source, we need a config that helps us find that.

Example entry for The Washington Post:
```json
  "washingtonpost.com":{
    "articleDivClass":"article-body",
      "urlIgnoreList":[
        "hosted-washpost.submissionplatform.com",
        "wapo.st"
      ]
  }
```
This entry indicates that for pages under the **washingtonpost.com** domain, articles can be found in a div with class **article-body**. In addition to the top-level domain, there are a few domains that the Washington Post uses to link back to itself (such as **wapo.st**) that we should not consider to be external links

Other options include adding a divQuerySelectorIgnoreList, indicating that there are some divs included inside the article content that shouldn't be included in the counts for different links.  An example of this is a div on Quillette with social media share buttons:

```json
  "quillette.com": {
    "articleQuerySelector": ".entry-content",
    "divQuerySelectorIgnoreList" : 
    [
      ".sharedaddy",
      ".jp-relatedposts"
    ]
  }
```


#### embeds.json
There are some embeds (like Tweets) that need some extra parsing due to Shadow DOM or other quirks.  Videos might also need extra work here.  NPR in particular likes to embed pdfs in the page.  

```json
    {
        "name":"Twitter",
        "config":
        {
            "embedQuerySelector":"twitter-widget",
            "linkQuerySelector":".long-permalink"
        }
    }
```
This example contains the query selectors for finding the Twitter embed and finding the link inside of it.

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)

## Attributions
Code to generate charts in the popup is all from [Chart.js](https://www.chartjs.org/)
Icons made by [Eucalyp](https://www.flaticon.com/authors/eucalyp) from [www.flaticon.com](https://www.flaticon.com/)