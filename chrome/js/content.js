/** content.js
 * 
 * This is the main content script for Primary Source Scraper
 * 
 * This file scans the current page for links and initiates PostMessages to other scripts to update UI:
 *  - badge.js to update the Badge text and color on the PSS Icon
 *  - popup.js to display the link analysis results in a graph
 * 
 */

var config;
var embeddedLinksConfig;
var hostname;
var settings = new UserSettings();
var selecting = false;
var articleAnalysis = undefined;

/* #region - Load User Settings and App Settings **/

function loadConfig() {
    // Load up the Embedded Links config - this can run later
    fetch(chrome.extension.getURL('./settings/embeds.json'))
        .then((resp) => resp.json())
        .then(function (jsonData) {
            embeddedLinksConfig = jsonData;
        });

    return UserSettings.fetch()
        .then(userSettings => settings = userSettings)
        .then(() => {
            return new Promise((resolve, reject) => {
                // Find the hostname
                hostname = utils.getHostNameFromUrl(window.location);

                // Look for a meta tag that advertises support for PSS
                var pssMetaTag = document.querySelector("meta[name='pss_selectors']");
                if (pssMetaTag)
                {
                    try
                    {
                        // If this looks like a valid PSS config, use it.
                        var configData = JSON.parse(pssMetaTag.content);

                        if (!configData || !configData.articleQuerySelector)
                        {
                            throw ("Invalid Site Meta Tag");
                        }

                        configData.siteProvidedConfig = true;
                        configData.unsupported = false;
                        resolve(configData);
                        return;
                    } catch (e)
                    {
                        utils.warn("Site Meta Tag Was Bad")();
                    }
                }

                // Grab the config for the current hostname and return it
                fetch(chrome.extension.getURL('./settings/newsSources.json'))
                    .then((resp) => resp.json())
                    .then(function (jsonData) {
                        var configData = jsonData[hostname];
                        if (configData)
                        {
                            configData.siteProvidedConfig = false;
                            configData.unsupported = false;
                        }
                        // If there is no config, we can try to find the <article> tag and use that.
                        // Only enabled if the user has enabled optimistic parsing
                        else if (settings.optimisticParsing)
                        {
                            configData = {
                                articleQuerySelector: "article",
                                siteProvidedConfig: false,
                                unsupported: true
                            };
                        }
                        resolve(configData);
                    });
            });
        });
}

/* #endregion - Load User Settings and App Settings **/

/* #region - Link Analysis **/

function tryAnalyzeCurrentPage(forceFullRescan = false) {
    hostname = utils.getHostNameFromUrl(window.location);
    return UserSettings.fetch(true)
        .then(userSettings => {
            try
            {
                settings = userSettings;
                if (!(config && config.siteProvidedConfig) && settings.isIgnoredDomain(hostname))
                {
                    utils.log("Ignored site: ")(hostname);
                    chrome.runtime.sendMessage({ message: constants.postMessages.SiteIsInIgnoreList, hostname: hostname });
                    return;
                }

                if (!config)
                {
                    utils.log("No config exists for this site")();
                    chrome.runtime.sendMessage({ message: constants.postMessages.SiteNotSupported, hostname: hostname });

                } else
                {
                    if (forceFullRescan == true || !articleAnalysis || document.location != articleAnalysis.location)
                    {
                        articleAnalysis = new ArticleAnalysis(document, config, embeddedLinksConfig, userSettings);
                    }
                    articleAnalysis.parseArticleData();
                }

            }
            catch (error)
            {
                articleAnalysis.uploadErrorReport("GenericParsingFailure", error.stack);
            }
        });
}

/* #endregion - Link Analysis **/

/* #region - Custom Div Selection **/

function showCustomHighlightModal() {
    return new Promise((resolve, reject) => {
        var modal = document.createElement("div");
        modal.classList.add('pss_modal');
        modal.innerHTML = modalHTML;
        document.body.appendChild(modal);

        var style = document.createElement('style');
        if (style.styleSheet)
        {
            style.styleSheet.cssText = modalStyle;
        } else
        {
            style.appendChild(document.createTextNode(modalStyle));
        }
        document.getElementsByTagName('head')[0].appendChild(style);

        // Get the <span> element that closes the modal
        var closeButton = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        closeButton.onclick = function () {
            modal.style.display = "none";
            resolve(true);
        }

        var scanEntirePageButton = document.getElementById("pss_scanEntirePage");
        scanEntirePageButton.onclick = function () {
            modal.style.display = "none";
            resolve(false);
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal)
            {
                modal.style.display = "none";
                resolve(true);
            }
        }
    });
}

function highlightCustomDiv() {
    var modalCheck = document.querySelector('.pss_modal');
    var outerCheck = document.querySelector('.pss_outer');
    if (modalCheck == null || modalCheck?.style.display == "none")
    {
        selecting = false;
        if (outerCheck)
        {
            outerCheck.style.display = "none";
        }
        showCustomHighlightModal().then(result => {
            if (result)
            {
                selecting = true;

                var box = outerCheck ?? document.createElement("div");
                box.classList.add("pss_outer");
                box.style.display = "none";
                box.style.position = "absolute";
                box.style.zIndex = "65000";
                box.style.background = "rgba(0, 191, 255, .3)";

                document.body.appendChild(box);

                var mouseX, mouseY, target, lastTarget;

                // in case you need to support older browsers use a requestAnimationFrame polyfill
                // e.g: https://gist.github.com/paulirish/1579671
                window.requestAnimationFrame(function frame() {
                    window.requestAnimationFrame(frame);
                    if (selecting)
                    {
                        if (target && (target.className === "pss_outer" || target.className == "pss_modal"))
                        {
                            box.style.display = "none";
                            target = document.elementFromPoint(mouseX, mouseY);
                        }
                        box.style.display = "";

                        if (target === lastTarget) return;

                        lastTarget = target;
                        var rect = target.getClientRects();

                        box.style.width = (target.offsetWidth - 1) + "px";
                        box.style.height = (target.offsetHeight - 1) + "px";
                        box.style.left = (rect[0].x) + "px";
                        box.style.top = (rect[0].y + window.pageYOffset) + "px";
                    }
                });


                document.body.addEventListener("mousemove", function (e) {
                    mouseX = e.clientX;
                    mouseY = e.clientY;
                    target = e.target;
                });

                box.addEventListener('click', function (e) {

                    if (lastTarget.getAttribute("id"))
                    {
                        configQueryString = "[id=" + lastTarget.getAttribute("id") + "]";
                    } else
                    {
                        var iterator = lastTarget.classList.values();
                        var configQueryString = "";

                        for (var value of iterator)
                        {
                            configQueryString += '.' + value;
                        }
                    }

                    if (configQueryString == "")
                    {
                        configQueryString = "*";
                    }

                    utils.log("Selecting div with query string: ")(configQueryString);
                    selecting = false;

                    document.body.removeChild(box);
                    var modaldialog = document.querySelector('.pss_modal');
                    document.body.removeChild(modaldialog);

                    config = { articleQuerySelector: configQueryString, unsupported: true, siteProvidedConfig: false };
                    tryAnalyzeCurrentPage(true);

                });
            }
            else
            {
                config = { articleQuerySelector: "*", unsupported: true, siteProvidedConfig: false };
                tryAnalyzeCurrentPage(true);
            }
        });
    }
}

/* #endregion - Custom Div Selection **/

/* #region - PostMessages **/

function handlePostMessages_content(request, sender, sendResponse) {
    if (request.message == constants.postMessages.ScanUnsupportedSite)
    {
        config = { articleQuerySelector: "*", unsupported: true, siteProvidedConfig: false };
        tryAnalyzeCurrentPage();
    }
    if (request.message == constants.postMessages.RequestLinkAnalysis)
    {
        tryAnalyzeCurrentPage();
    } 
    else if (request.message == constants.postMessages.HighlightLinksFromDomain)
    {
        this.articleAnalysis.setLinkHighlightingStrategy(ArticleAnalysis.DomainSpecificLinkHighlightingStrategy(request.domain, request.color));
        this.articleAnalysis.scrollToLinkIndex(request.domain, request.index);
    } 
    else if (request.message == constants.postMessages.HighlightLinksInCategory)
    {
        this.articleAnalysis.setLinkHighlightingStrategy(ArticleAnalysis.CategorySpecificLinkHighlightingStrategy(request.category, request.color));
    }
    else if (request.message == constants.postMessages.CustomDivSelection)
    {
        highlightCustomDiv();
    }
}

/* #endregion - PostMessages **/


/** SCRIPT LOAD **/

function startPSS() {
    loadConfig().then(function (jsonData) {
        config = jsonData;
        if (settings.autoHighlight)
        {
            // Most pages will probably not actually mess with the DOM after loading.
            // Wait a second, then scan for sources.
            pageLoadTimeout = setTimeout(function () {
                tryAnalyzeCurrentPage();
            }, 1000);

            // Some pages have Twitter embeds, which mess with the DOM for a while.
            // For these, delay the scan until it looks like they're done.
            const callback = function (mutationsList, observer) {
                // Use traditional 'for loops' for IE 11
                for (let mutation of mutationsList)
                {
                    if (mutation.type === 'subtree' || mutation.type == 'childList')
                    {
                        clearTimeout(pageLoadTimeout);
                        pageLoadTimeout = setTimeout(function () {
                            tryAnalyzeCurrentPage();
                        }, 1000);
                    }
                }
            };

            if (config)
            {
                article = ArticleAnalysis.findArticleOnPage(document, config);
                if (!article)
                {
                    return;
                }
                // Create an observer instance linked to the callback function
                const observer = new MutationObserver(callback);

                // Start observing the target node for configured mutations
                const observerConfig = { childList: true, subtree: true }
                observer.observe(article, observerConfig);
            }
        }
    });
}

chrome.runtime.onMessage.addListener(handlePostMessages_content);
startPSS();

// This is gross, but this appears to be the only way to handle URL navigations to the same hostname.
var currentURL = location.href;
setInterval(function () {
    if (location.href != currentURL)
    {
        startPSS();
        currentURL = location.href
    }
}, 1000);