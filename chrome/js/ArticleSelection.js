
const modalStyle = `
/* The Modal (background) */
.pss_modal {
  display: block; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 9999; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  font-family: "Segoe UI", Tahoma, sans-serif;
  font-size: 75%;
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 20px;
  font-weight: bold;
  bottom: 3px;
  position: relative;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
} 

#pss_scanEntirePage
{
    float: right;
    text-decoration: underline;
    text-underline-offset: 2px;
}

#pss_scanEntirePage:hover,
#pss_scanEntirePage:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
} 

/* Modal Header */
.pss_modal-header {
  padding: 2px 16px;
  background-color: #1f77b4;
  color: white;
  font-size: 22px;
}

/* Modal Body */
.pss_modal-body {padding: 2px 16px; font-size:14px}

/* Modal Footer */
.pss_modal-footer {
  padding: 5px 16px;
  background-color: #1f77b4;
  color: white;
  text-align: center;
  font-size: 14px;
  height: 20px;
  box-sizing: initial;
}

/* Modal Content */
.pss_modal-content {
  display: block;
  position: relative;
  background-color: #fefefe;
  margin: 15% auto;
  padding: 0;
  border: 1px solid #888;
  width: 60%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@keyframes animatetop {
  from {top: -10%; opacity: 0}
  to {top: 0; opacity: 1}
} 
`

const modalHTML = `
<div class="pss_modal-content">
  <div class="pss_modal-header">
    <span class="close">&times;</span>
    Article Select
  </div>
  <div class="pss_modal-body">
    <p>Move the highlighted box so it contains the article on this page, then click on the article.</p>
    <p>Click outside this popup to continue.</p>
  </div>
  <div class="pss_modal-footer">
    <div style="float:left">Primary Source Scraper</div>
    <div id="pss_scanEntirePage">Scan Entire Page Instead</div>
  </div>
</div>`
