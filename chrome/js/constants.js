/** constants.js
 *
 * This file contains common constants used across the extension
 */
var constants = {
	postMessages: {
		CustomDivSelection: "CustomDivSelection",
		HighlightLinksFromDomain: "HighlightLinksFromDomain",
		HighlightLinksInCategory: "HighlightLinksInCategory",
		LinkAnalysisResult: "LinkAnalysisResult",
		RequestLinkAnalysis: "RequestLinkAnalysis",
		ScanUnsupportedSite: "ScanUnsupportedSite",
		SiteIsInIgnoreList: "SiteIsInIgnoreList",
		SiteNotSupported: "SiteNotSupported",
		NewArticleFound: "NewArticleFound",
	},
	colors: {
		BadgeDefault: "#4285F4",
		BadgeNewArticle: "#23780c",
		BadgeUnsupported: "#cc0000",
		HighlighterDefault: "#00BFFF",
		InternalLinks: "#888",
		SocialLinks: "#1f77b4",
		PrimarySourceLinks: "#23780c",
		NewsSourceLinks: "#FF0000",
		MiscLinks: "#CCC",
		WarningMessage: "#ffd000",
		NewArticleMessage: "#23780c",
		LightText: "#fcfcfc",
	},
	urls: {
		pssIngestAPI:
			"https://primarysourcescraper.azurewebsites.net/api/PrimarySourceScraper",
	},
	strings: {
		Internal: "This Site (Internal)",
		SocialMedia: "Social Media",
		NewsSite: "News / Opinion",
		Misc: "Misc",
		PrimarySource : "Primary Sources"
	},
	aka : {
		"amzn.to" : "amazon.com",
		"bbc.co.uk" : "bbc.com",
		"t.co" : "twitter.com",
		"t.me" : "telegram.org",
		"youtu.be" : "youtube.com"
	},
	versions: 
	{
		current: "0.7.7",
		lastPrivacyUpdateVersion: "0.7.4"
	}
};
