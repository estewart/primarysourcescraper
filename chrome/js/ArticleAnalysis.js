
class ArticleAnalysis {

    constructor(document, config, embeddedLinkConfig, userSettings) {
        this.document = document;
        this.location = document.location;
        this.config = config;
        this.embeddedLinkConfig = embeddedLinkConfig;
        this.userSettings = userSettings;

        this.headless = typeof (window) == 'undefined';
        this.newArticle = false;
        this.articleData = undefined;
        this.sentErrorData = false;

        this.linkHighlightStrategy = ArticleAnalysis.DefaultLinkHighlightingStrategy;
    }

    static findArticleOnPage(document, config) {
        return document.querySelector(config["articleQuerySelector"]);
    }

    findArticle() {
        var article = ArticleAnalysis.findArticleOnPage(this.document, this.config);
        var isHomePage = this.location.pathname == "/";
        if (!article)
        {
            utils.log("Couldn't find Article")();
            if (!this.headless)
            {
                this.sendMessage({ message: constants.postMessages.ArticleNotFound, isHomePage: isHomePage });
            }
        }
        return article;
    }

    parseArticleData() {

        if(!this.config)
        {
            return Promise.resolve({error:"Unsupported Site"});
        }
        if (!this.findArticle())
        {
            // If we couldn't find the article, but we had a real config for this site, we should report that error.
            if(this.config.unsupported == false && this.location.pathname != "/")
            {
                return this.uploadErrorReport("QuerySelectorMismatch", this.config["articleQuerySelector"]);
            }
            else
            {
                return Promise.resolve({error:"Couldn't Find Article"});
            }
        }

        if (this.articleData && this.articleData.article.location == this.location && !this.config.unsupported)
        {
            this.parseArticleLinks();
            this.sendMessage({ message: constants.postMessages.LinkAnalysisResult, result: this.articleData, unsupported: this.config.unsupported });
            if (this.newArticle)
            {
                this.sendMessage({ message: constants.postMessages.NewArticleFound, text: this.articleData.externalLinks.length });
            }
            return Promise.resolve(this.articleData);
        }

        var articleLinks = this.parseArticleLinks();
        var externalLinks = articleLinks.externalLinks;
        var articleDate = this.parseArticleDate();

        this.articleData = new ArticleData(
            this.document.title,
            articleDate,
            this.document.location.href,
            articleLinks.allLinks.length,
            externalLinks);

        utils.log("Article Data: ")(this.articleData);
        this.sendMessage({ message: constants.postMessages.LinkAnalysisResult, result: this.articleData, unsupported: this.config.unsupported });

        if (this.config.unsupported == false)
        {
            return this.uploadArticleData();
        } else
        {
            return Promise.resolve(false);
        }
    }

    parseArticleLinks() {

        var allLinks = this.getAllRelevantLinksInDocument();

        // Build a list of urls we don't count as "external"
        var ignoreableUrls = this.config["urlIgnoreList"];
        if (!ignoreableUrls)
        {
            ignoreableUrls = [];
        }
        var cleanHostname = utils.getHostNameFromUrl(this.document.location);
        ignoreableUrls.push(cleanHostname);

        var externalLinks = this.findExternalLinks(allLinks, ignoreableUrls);
        var embeddedLinks = this.findEmbeddedLinks();

        // Include the embedded links in the external links list (and all links)
        embeddedLinks.forEach(embeddedLink => {
            externalLinks.push(embeddedLink);
            allLinks.push(embeddedLink);
            utils.log("Added ")(embeddedLink);
        });

        return { allLinks: allLinks, externalLinks: externalLinks };
    }

    parseArticleDate() {
        if (this.config["articleDateQuerySelector"])
        {
            if (this.document.querySelector(this.config["articleDateQuerySelector"]))
            {
                return this.document.querySelector(this.config["articleDateQuerySelector"]).innerText;
            }
        }
        return "";
    }

    getAllRelevantLinksInDocument() {
        // Find all the links in the article
        var allLinks = this.document.querySelectorAll(this.config["articleQuerySelector"] + " a[href]");
        allLinks = Array.prototype.slice.call(allLinks);

        var linksToRemove = [];

        allLinks.forEach(link => {
            if (!link["href"] || link["href"].startsWith("mailto") || link["href"].startsWith("javascript:") ||  link["href"].startsWith("tel:"))
            {
                linksToRemove.push(link);
            }
        });

        // Separate loop to remove the links so we don't run into issues with removing during an iterator
        linksToRemove.forEach(link => {
            allLinks.splice(allLinks.indexOf(link), 1);
        });

        // Remove any links that are in ignoreable divs
        if (this.config["divQuerySelectorIgnoreList"])
        {
            this.config["divQuerySelectorIgnoreList"].forEach(divSelector => {
                var ignoreableLinks = this.document.querySelectorAll(divSelector + " a[href]");
                ignoreableLinks.forEach(link => {
                    if (allLinks.indexOf(link) > -1)
                    {
                        allLinks.splice(allLinks.indexOf(link), 1);
                    }
                });
            });
        }
        return allLinks;
    }

    findExternalLinks(links, ignoreableUrls) {
        var externalLinks = [];
        links.forEach(link => {
            var ignore = false;
            ignoreableUrls.forEach(url => {
                if ((link["href"] == undefined) || link["href"].includes(url))
                {
                    ignore = true;
                }
            });
            if (!ignore)
            {
                this.highlightLink(link);
                externalLinks.push(new ExternalLink(link["href"], (link.text == undefined) ? "" : link.text.trim()));
            }
        });
        return externalLinks;
    }

    findEmbeddedLinks(domain = null, color = this.userSettings.highlighterColor, index = 0, linkIndex = 0) {
        var embeddedLinks = [];
        this.embeddedLinkConfig.forEach(embedType => {
            var embedConfig = embedType.config;
            if (embedConfig)
            {
                // Find any embeds of this type
                var embeddedObjects = this.document.querySelectorAll(embedConfig["embedQuerySelector"]);

                var embedIndex = 0;
                // Highlight the iFrame or div containing the embed
                embeddedObjects.forEach(object => {
                    var link = object;
                    embeddedLinks.push(new ExternalLink(embedType.url + embedIndex, embedType.name));
                    if (domain && embedType.url.includes(domain))
                    {
                        if (linkIndex == index)
                        {
                            link.scrollIntoView(false);
                            window.scrollBy(0, window.outerHeight / 2);
                        }
                        linkIndex++;
                        this.highlightEmbed(link, color);
                    } else
                    {
                        this.highlightEmbed(link, null);
                    }

                    if (!domain)
                    {
                        this.highlightEmbed(link, color);
                    }
                    embedIndex++;

                });
            }
        });
        return embeddedLinks;
    }

    sendMessage(message) {
        if (!this.headless)
        {
            chrome.runtime.sendMessage(message);
        }
    }

    highlightEmbed(embed, color = this.userSettings.highlighterColor) {
        if (!this.headless)
        {
            if (color == null)
            {
                embed.style.border = "1px solid gray";
            } else
            {
                embed.style.border = "5px solid " + color;
            }
        }
    }

    setLinkHighlightingStrategy(strategyFn)
    {
        this.linkHighlightStrategy = strategyFn;
        var allLinks = this.getAllRelevantLinksInDocument();
        allLinks.forEach(link => {
            this.highlightLink(link)
        });
    }

    highlightLink(link, color = this.userSettings.highlighterColor, forceUpdate = false) {
        if (!this.headless)
        {
            var linkColor = this.linkHighlightStrategy(link, this.userSettings, utils.getHostNameFromUrl(this.location));

            link.style.backgroundColor = linkColor;
            link.style.color = linkColor == null ? "unset" : utils.invertColor(linkColor, false);
        }
    }

    scrollToLinkIndex(domain, index = 0)
    {
        var allLinks = this.getAllRelevantLinksInDocument();

        var linkIndex = 0;
        allLinks.forEach(link => {
            var cleanLink = utils.cleanUrl(new URL(link["href"]));
            if (cleanLink.includes(domain))
            {
                if (linkIndex == index)
                {
                    link.scrollIntoView(false);
                    window.scrollBy(0, window.outerHeight / 2);
                }
                linkIndex++;
            }
        });
    }

    uploadArticleData() {
        if (!this.articleData || this.userSettings.sendArticleData == false)
        {
            utils.log("Not Uploading Article Data")();
            return Promise.resolve(false);
        }

        return new Promise((resolve, reject) => {

            const requestOptions = {
                method: 'POST',
                body: JSON.stringify(this.articleData),
                headers: { 'Content-Type': 'application/json' }
            };

            fetch(constants.urls.pssIngestAPI, requestOptions)
                .then(res => res.json())
                .then(res => {
                    if (res && res.NewArticle == true)
                    {
                        this.newArticle = true;
                        if (!this.headless)
                        {
                            this.userSettings.incrementNewArticleCount();
                        }
                        this.sendMessage({ message: constants.postMessages.NewArticleFound, text: this.articleData.externalLinks.length });
                    }
                    utils.log("Uploaded Article Data.  Result: ")(res);
                    resolve(res);
                });
        });

    }

    uploadErrorReport(errorType, errorInfo) {
        if (this.userSettings.sendErrorReports == false || this.sentErrorData == true)
        {
            utils.log("Not Uploading Error Report")();
            return Promise.resolve(false);
        }

        const errorReport = new ErrorReport(errorType, errorInfo, this.location.href, constants.versions.current);

        return new Promise((resolve, reject) => {

            const requestOptions = {
                method: 'POST',
                body: JSON.stringify(errorReport),
                headers: { 'Content-Type': 'application/json' }
            };

            fetch(constants.urls.pssIngestAPI, requestOptions)
                .then(res => {
                    this.sentErrorData = true;
                    utils.log("Uploaded Error Report")({errorType, errorInfo}); 
                    resolve({error: errorType});
                });
        });

    }

    static DefaultLinkHighlightingStrategy(link, userSettings, currentDomain)
    {
        if(userSettings.onlyHighlightPrimarySources)
        {
            return ArticleAnalysis.CategorySpecificLinkHighlightingStrategy(constants.strings.PrimarySource)(link, userSettings)
        }
        else{
            return userSettings.highlighterColor;
        }
    }

    static DomainSpecificLinkHighlightingStrategy(domain, color){
        return function (link, userSettings, currentDomain)
        {
            var cleanLink = utils.cleanUrl(new URL(link["href"]));
            if(cleanLink.includes(domain))
            {
                return color;
            }

            return null;
        }
    }

    static CategorySpecificLinkHighlightingStrategy(category, color = null){
        return function (link, userSettings, currentDomain)
        {

            if(color == null)
            {
                color = userSettings.highlighterColor;
            }

            // Check for internal vs. external link
            // If internal, hide unless we're explicitly showing internal links
            var hostname = utils.getHostNameFromUrl(new URL(link));
            if(hostname.includes(currentDomain))
            {
                if(category != constants.strings.Internal)
                {
                    return null;
                }
                else
                {
                    return color;
                }  
            }

            // If there's no category set just highlight all links
            if(category == null)
            {
                return color;
            }
                
            var siteCategory = userSettings.getSiteCategory(hostname);
            if(siteCategory == category)
            {
                return color;
            }

            return null;
        }
    } 
}