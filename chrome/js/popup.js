/** popup.js
 * 
 * This file manages the Popup UI when users click on the PSS Icon.  This file acts on popup.html.
 * 
 * This contains all the integration with Chart.js to display the pie charts / bar graph for link analysis.
 */

// From ChartJs-Plugin-ColorSchemes.js
var Classic20 = ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'];
var externalLinkRatioChart;
var externalLinkDetailsChart;
var newArticle;
var defaultFilterCategories = [constants.strings.SocialMedia, constants.strings.NewsSite, constants.strings.PrimarySource, constants.strings.Misc];
var currentFilterCategories = [...defaultFilterCategories];
var chartData;

/* #region - Popup-specific constants **/

var popupMessages = {
    IgnoredSite: {
        message: "You've marked this as a site to ignore.",
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "info"
    },
    ArticleNotFound: {
        message: "No Article Found",
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "info"
    },
    NoLinks: {
        message: 'There are no links in this article.',
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "info"
    },
    NoExternalLinks: {
        message: 'There are no external links in this article.',
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "info"
    },
    SiteNotSupported: {
        message: 'This site is not yet supported\nResults may be inaccurate',
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "warning"
    },
    FoundNewArticle: {
        message: "You found a new article!",
        color: constants.colors.NewArticleMessage,
        textColor: constants.colors.LightText,
        icon: "verified"
    },
    AutoHighlightDisabled: {
        message: "Auto-Highlight has been disabled",
        color: constants.colors.WarningMessage,
        textColor: "black",
        icon: "info"
    },
    FirstRun: {
        message: "Please review our Privacy Settings\n(Available in the Options menu)",
        color: constants.colors.NewArticleMessage,
        textColor: constants.colors.LightText,
        icon: "privacy_tip"
    },
}

/* #endregion - Popup-specific constants **/

/* #region - Popup Graph Display **/

function getPieChartColor(context) {
    return Classic20[context.dataIndex % Classic20.length];
}

function drawCharts(linkAnalysisResult, unsupported) {
    if (externalLinkDetailsChart)
    {
        externalLinkDetailsChart.destroy();
    }

    var articleHostname = utils.getHostNameFromUrl(new URL(linkAnalysisResult.article.location));
    UserSettings.fetch().then(settings => {
        if (settings.ignoredDomains.includes(articleHostname))
        {
            showErrorMessage(articleHostname);
            return;
        }

        hideErrorMessage();
        if (unsupported)
        {
            showUnsupportedDisclaimer(articleHostname);
        }
        else if (settings.lastPrivacyUpdateVersion < constants.versions.lastPrivacyUpdateVersion)
        {
            showMessageBar(popupMessages.FirstRun);
        } else if (newArticle)
        {
            showNewArticleMessage();
        }

        chartData = new ChartData(articleHostname, linkAnalysisResult, settings);
        Chart.defaults.global.defaultFontFamily = '"Segoe UI", Tahoma, sans-serif';

        if (linkAnalysisResult.totalLinkCount == 0)
        {
            setChartCanvasDisplayed(false, false);
            showMessageBar(popupMessages.NoLinks);
            return;
        } else
        {
            drawLinkCategoriesChart();
        }


        if (chartData.externalLinkCount == 0)
        {
            setChartCanvasDisplayed(true, false);
            showMessageBar(popupMessages.NoExternalLinks);
        } else
        {
            drawLinkDetailsChart();
        }
    });
}

function drawLinkCategoriesChart() {

    var hiddenIndices = [];
    if (externalLinkRatioChart)
    {
        externalLinkRatioChart.getDatasetMeta(0).data.forEach(category => {
            if (externalLinkRatioChart.getDatasetMeta(0).data[category._index].hidden)
            {
                hiddenIndices.push(category._index);
            }
        });
        externalLinkRatioChart.destroy();
    }

    var ctx = document.getElementById('externalLinkRatio').getContext('2d');
    externalLinkRatioChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: chartData.ratioLabels,
            datasets: [{
                label: '# of Links',
                data: chartData.ratioData,
                backgroundColor: [
                    constants.colors.InternalLinks,
                    constants.colors.SocialLinks,
                    constants.colors.NewsSourceLinks,
                    constants.colors.PrimarySourceLinks,
                    constants.colors.MiscLinks]
            }]
        },
        options: {
            title:
            {
                display: true,
                text: 'Link Categories'
            },
            legend: {
                position: 'right',
                labels: {
                    boxWidth: 10,
                    fontSize: 11,
                    padding: 5
                },
                onClick: onLinkCategoryClicked
            }
        }
    });

    // Refresh the chart with any current filters we have
    if (hiddenIndices.length > 0)
    {
        hiddenIndices.forEach(index => {
            externalLinkRatioChart.getDatasetMeta(0).data[index].hidden = true;
        });
        externalLinkRatioChart.update();
    }

    var internalLinkIndex = 0;
    document.getElementById("externalLinkRatio").onclick = function (evt) {
        if (externalLinkRatioChart)
        {
            var activePoints = externalLinkRatioChart.getElementsAtEvent(evt);

            if (activePoints.length > 0)
            {
                //get the internal index of slice in pie chart
                var clickedElementIndex = activePoints[0]["_index"];

                //get specific label by index 
                var label = externalLinkRatioChart.data.labels[clickedElementIndex];
                var value = externalLinkRatioChart.data.datasets[0].data[clickedElementIndex];

                if (label.includes(constants.strings.Internal))
                {
                    chrome.tabs.query({
                        active: true,
                        currentWindow: true
                    }, function (tabs) {
                        chrome.tabs.sendMessage(tabs[0].id, {
                            message: constants.postMessages.HighlightLinksInCategory,
                            category: constants.strings.Internal,
                            color: constants.colors.InternalLinks
                        });
                        internalLinkIndex = (internalLinkIndex + 1) % value;
                    });
                }
                else
                {
                    if (currentFilterCategories.length == 1 && currentFilterCategories[0] == label)
                    {
                        clearAllCategoryFilters();
                    } else
                    {
                        deselectAllOtherCategories(label);
                    }
                }
            }
        }
    }
}

function drawLinkDetailsChart() {

    UserSettings.fetch().then(settings => {
        if (externalLinkDetailsChart)
        {
            externalLinkDetailsChart.destroy();
        }

        sortedLabels = [...chartData.detailLabels];
        sortedData = [...chartData.detailData];
        sortedTotalCount = [...chartData.detailMetadata];

        // Filter out any unselected categories
        var toRemove = [];
        sortedLabels.forEach(hostname => {
            if (!currentFilterCategories.includes(settings.getSiteCategory(hostname)))
            {
                toRemove.push(sortedLabels.indexOf(hostname));
            }
        });
        toRemove = toRemove.reverse();
        toRemove.forEach(index => {
            sortedLabels.splice(index, 1);
            sortedData.splice(index, 1);
            sortedTotalCount.splice(index, 1);
        });


        var ctx = document.getElementById('externalLinkDetails').getContext('2d');
        if (sortedLabels.length <= 6)
        {
            externalLinkDetailsChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: sortedLabels,
                    datasets: [{
                        label: '# of Links',
                        data: sortedData,
                        backgroundColor: getPieChartColor
                    }]
                },
                options: {
                    legend: {
                        position: 'top',
                        labels: {
                            boxWidth: 20,
                            fontSize: 11,
                            padding: 5
                        },
                        onClick: showContextForDomain
                    },
                    title: {
                        display: true,
                        text: 'Sources'
                    }
                }
            });
        } else
        {
            externalLinkDetailsChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels: sortedLabels,
                    datasets: [{
                        minBarLength: 2,
                        label: '# of Links',
                        data: sortedData,
                        backgroundColor: getPieChartColor
                    }]
                },
                options: {
                    legend: { display: false },
                    scales: {
                        xAxes: [{
                            ticks: {
                                min: 0
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'Sources'
                    }
                }
            });
        }

        var linkIndex = 0;
        var lastClickedLabel;
        document.getElementById("externalLinkDetails").onclick = function (evt) {
            if (externalLinkDetailsChart)
            {
                var activePoints = externalLinkDetailsChart.getElementsAtEvent(evt);

                if (activePoints.length > 0)
                {
                    //get the internal index of slice in pie chart
                    var clickedElementIndex = activePoints[0]["_index"];

                    //get specific label by index 
                    var label = externalLinkDetailsChart.data.labels[clickedElementIndex];

                    if (!evt.fromPopupLabel)
                    {
                        if (!lastClickedLabel)
                        {
                            lastClickedLabel = label;
                        }
                        if (lastClickedLabel != label)
                        {
                            lastClickedLabel = label;
                            linkIndex = 0;
                        }

                        //get value by index      
                        var value = sortedTotalCount[clickedElementIndex];
                        var color = utils.rgbToHex(activePoints[0]._view.backgroundColor);

                        chrome.tabs.query({
                            active: true,
                            currentWindow: true
                        }, function (tabs) {
                            chrome.tabs.sendMessage(tabs[0].id, {
                                message: constants.postMessages.HighlightLinksFromDomain,
                                domain: label,
                                color: color,
                                index: linkIndex
                            });
                            linkIndex = (linkIndex + 1) % value;
                        });
                    } else
                    {
                        showContextForDomain(evt, { index: clickedElementIndex, text: externalLinkDetailsChart.data.labels[clickedElementIndex] })
                    }
                } else
                {
                    // Welp, the click landed not on part of the bar chart.
                    // We need to see if this was a click on a label - but the label doesn't have a click handler.

                    // If the context menu is already shown, don't do anything
                    if (event.contextShown)
                    { return; }

                    // Dispatch a new click 10px to the right and see if we can hit the bar - if we do, mark it so we know this was
                    // an event we dispatched to get the label property out of the graph
                    var newEvt = new MouseEvent("click",
                        {
                            clientX: evt.clientX + 10,
                            clientY: evt.clientY,
                            screenX: evt.screenX + 10,
                            screenY: evt.screenY,
                        });

                    // mark this as an event potentially from a popup label
                    newEvt.fromPopupLabel = true;

                    // Don't seek further than 150px (10px each time * 15 times).
                    newEvt.recurseCount = (evt.recurseCount ?? 0) + 1;
                    if (newEvt.recurseCount < 15)
                    {
                        document.getElementById("externalLinkDetails").dispatchEvent(newEvt);
                    }
                }
            }
        }
    });
}

/* #endregion - Popup Graph Display **/

/* #region - UI Updates **/

function initPopupUI() {
    UserSettings.fetch().then(settings => {
        var autoHighlight = document.getElementById('autoHighlight');
        autoHighlight.addEventListener('change', (event) => {
            settings.autoHighlight = !autoHighlight.checked;
            settings.commit();
            if (settings.autoHighlight)
            {
                hideMessageBar();
            }
            else
            {
                showMessageBar(popupMessages.AutoHighlightDisabled);
            }
        });
    });
    resetPopupUI();
}

function resetPopupUI() {
    var detailsPane = document.getElementById('externalLinkDetails');
    var ratioPane = document.getElementById('externalLinkRatio');
    var leftButton = document.getElementById('leftButton');
    var rightButton = document.getElementById("rightButton");

    detailsPane.style.display = "none";
    ratioPane.style.display = "none";
    leftButton.style.display = "none";
    leftButton.innerText = "";
    rightButton.style.display = "none"
    rightButton.innerText = "";

    UserSettings.fetch().then(settings => {
        var autoHighlight = document.getElementById('autoHighlight');
        autoHighlight.checked = !settings.autoHighlight;
    });

    hideMessageBar();
}

function showArticleNotFoundMessage(isHomePage) {
    resetPopupUI();
    showMessageBar(popupMessages.ArticleNotFound);

    var leftButton = document.getElementById('leftButton');

    if (isHomePage)
    {
        leftButton.style.display = "";
        leftButton.innerText = "Rescan Page";
        leftButton.onclick = rescanPage;
    } else
    {
        leftButton.style.display = "";
        leftButton.innerText = "Find Article";
        leftButton.onclick = customDivSelection;
    }
}

// This function handles the case when we forcefully scanned an unsupported page.
function showUnsupportedDisclaimer(hostname) {
    showMessageBar(popupMessages.SiteNotSupported);

    var leftButton = document.getElementById("leftButton");
    leftButton.style.display = "";
    leftButton.innerText = "Find Article"
    leftButton.onclick = customDivSelection;

    var rightButton = document.getElementById("rightButton");
    rightButton.style.display = "";
    rightButton.innerText = "Ignore " + hostname;
    rightButton.onclick = ignoreDomain;
}

function showErrorMessage(hostname) {
    resetPopupUI();

    var leftButton = document.getElementById('leftButton');
    leftButton.style.display = "";

    UserSettings.fetch().then(settings => {
        if (settings.isIgnoredDomain(hostname))
        {
            showMessageBar(popupMessages.IgnoredSite);
            leftButton.innerText = "Stop Ignoring " + hostname;
            leftButton.onclick = stopIgnoringDomain;
        } else
        {
            showUnsupportedDisclaimer(hostname);
        }
    });
}

function showNewArticleMessage() {
    if (!isMessageBarVisible())
    {
        showMessageBar(popupMessages.FoundNewArticle);
    }
}

function isMessageBarVisible() {
    var messageDiv = document.getElementById('messageBar');
    return messageDiv.style.display == "";
}

function showMessageBar(messageType) {
    var messageDiv = document.getElementById('messageBar');
    var innerText = messageDiv.firstElementChild;

    innerText.innerText = messageType.message;
    innerText.style.color = messageType.textColor;

    messageDiv.style.backgroundColor = messageType.color;
    messageDiv.style.display = "";

    var lineCount = messageType.message.split('\n').length;
    console.log(lineCount);

    var rightIcon = document.getElementById('rightIcon');
    var leftIcon = document.getElementById('leftIcon');
    if (messageType.icon)
    {

        rightIcon.textContent = messageType.icon;
        rightIcon.style.position = "absolute";
        rightIcon.style.right = "8px"
        rightIcon.style.top = (lineCount == 1) ? "40px" : "48px";
        rightIcon.style.color = messageType.textColor;

        leftIcon.textContent = messageType.icon;
        leftIcon.style.position = "absolute";
        leftIcon.style.left = "8px"
        leftIcon.style.top = (lineCount == 1) ? "40px" : "48px";
        leftIcon.style.color = messageType.textColor;
    }
    else
    {
        rightIcon.textContent = "";
        leftIcon.textContent = "";
    }

    if (messageType == popupMessages.FirstRun)
    {
        document.getElementById('options').classList.add('blink');
    }
}

function hideMessageBar() {
    var messageDiv = document.getElementById('messageBar');
    messageDiv.style.display = "none";
}

function setChartCanvasDisplayed(showRatioChart = true, showDetailsChart = true) {
    var detailsPane = document.getElementById('externalLinkDetails');
    var ratioPane = document.getElementById('externalLinkRatio');

    detailsPane.style.display = showDetailsChart ? "block" : "none";
    ratioPane.style.display = showRatioChart ? "block" : "none";
}

function hideErrorMessage() {
    resetPopupUI();
    setChartCanvasDisplayed();

    var leftButton = document.getElementById('leftButton');
    leftButton.style.display = "";
    leftButton.innerText = "Rescan Page";
    leftButton.onclick = rescanPage;
}

function ignoreDomain() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        utils.getCurrentHostName().then((articleHostname) => {
            UserSettings.fetch()
                .then(settings => settings.addIgnoredUrl(articleHostname))
                .then(rescanPage);
        });
    });
}

function stopIgnoringDomain() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        utils.getCurrentHostName().then((articleHostname) => {
            UserSettings.fetch()
                .then(settings => settings.removeIgnoredUrl(articleHostname))
                .then(rescanPage);
        });
    });
}

function onLinkCategoryClicked(event, legendItem) {
    var index = legendItem.index;
    var ci = externalLinkRatioChart;
    var meta = ci.getDatasetMeta(0).data[index];

    // See controller.isDatasetVisible comment
    meta.hidden = !meta.hidden;

    // We hid a dataset ... rerender the chart
    ci.update();

    currentFilterCategories = [...defaultFilterCategories];
    var indexesToRemove = [];
    ci.getDatasetMeta(0).data.forEach(category => {
        if (category._index != 0 && category.hidden)
        {
            indexesToRemove.push(category._index - 1);
        }
    });
    indexesToRemove = indexesToRemove.reverse();
    indexesToRemove.forEach(index => {
        currentFilterCategories.splice(index, 1);
    });

    drawLinkDetailsChart();
}


function deselectAllOtherCategories(chosenCategory) {

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.HighlightLinksInCategory,
            category: chosenCategory
        });
    });

    var chosenIndex = defaultFilterCategories.indexOf(chosenCategory) + 1;
    currentFilterCategories = [...defaultFilterCategories];
    var ci = externalLinkRatioChart;
    var indexesToRemove = [];
    ci.getDatasetMeta(0).data.forEach(category => {
        if (category._index != chosenIndex)
        {
            category.hidden = true;
            if (category._index != 0)
            {
                indexesToRemove.push(category._index - 1);
            }
        }
    });
    indexesToRemove = indexesToRemove.reverse();
    indexesToRemove.forEach(index => {
        currentFilterCategories.splice(index, 1);
    });
    ci.update();

    drawLinkDetailsChart();
}

function clearAllCategoryFilters() {

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.HighlightLinksInCategory,
            category: null
        });
    });

    currentFilterCategories = [...defaultFilterCategories];
    var ci = externalLinkRatioChart;
    ci.getDatasetMeta(0).data.forEach(category => {
        category.hidden = false;
    });
    ci.update();
    drawLinkDetailsChart();
}


function showContextForDomain(event, legendItem) {
    event.contextShown = true;
    closeContextMenus();
    contextDomain = legendItem.text;

    UserSettings.fetch().then(settings => {
        var chartPosition = document.getElementById('externalLinkDetails').getBoundingClientRect();
        var siteCategory = settings.getSiteCategory(contextDomain);
        var userDefinedCategory = settings.isUserDefinedSiteCategory(contextDomain);
        var contextMenu = document.getElementById('context' + legendItem.index);
        if (!contextMenu)
        {
            var dropdownHtml = "";
            dropdownHtml += "<div id='context" + legendItem.index + "' class='contextMenu' style='top:" + chartPosition.bottom + "px'>";
            dropdownHtml += "  <ul class='dropdown' style='display:block;position:static;margin-bottom:5px;'>";
            dropdownHtml += "<div class='contextTitle'>" + contextDomain + "<i style='float:right;position:relative;bottom:4px;left:12px' class='material-icons'>close</i></div>";

            if (userDefinedCategory || siteCategory == constants.strings.PrimarySource)
            {
                dropdownHtml += "    <a class='context_primarySource" + (siteCategory == constants.strings.PrimarySource ? " active" : "") + "'>Primary Source</a>";
                dropdownHtml += "    <a class='context_newsSite" + (siteCategory == constants.strings.NewsSite ? " active" : "") + "'>News / Opinion</a>";
                dropdownHtml += "    <a class='context_socialMedia" + (siteCategory == constants.strings.SocialMedia ? " active" : "") + "'>Social Media</a>";
                dropdownHtml += "    <a class='context_misc" + (siteCategory == constants.strings.Misc ? " active" : "") + "'>Misc</a>";
            } else
            {
                switch (siteCategory)
                {
                    case constants.strings.NewsSite:
                        dropdownHtml += "    <a class='context_newsSite active'>News / Opinion</a>";
                        break;
                    case constants.strings.SocialMedia:
                        dropdownHtml += "    <a class='context_socialMedia active'>Social Media</a>";
                        break;
                    case constants.strings.Misc:
                        dropdownHtml += "    <a class='context_misc active'>Misc</a>";
                        break;
                }
            }
            dropdownHtml += "    <a class='context_hideDomain'>Hide</a>";
            dropdownHtml += "  </ul>";
            dropdownHtml += "</div>";
            document.getElementById('contextMenus').innerHTML += dropdownHtml;
            contextMenu = document.getElementById('context' + legendItem.index);
        }
        contextMenu.style.display = 'block';
        contextMenu.style.position = 'absolute';

        var chartPosition = document.getElementById('externalLinkDetails').getBoundingClientRect();
        contextMenu.style.top = (chartPosition.top + 47) + "px";
        contextMenu.style.left = chartPosition.left + "px";
        contextMenu.style.width = (chartPosition.right - chartPosition.left) + "px";
        contextMenu.style.transition = "0.5s";


        if (userDefinedCategory || siteCategory == constants.strings.PrimarySource)
        {
            document.getElementsByClassName("context_primarySource")[0].addEventListener('click', () => { changeSiteCategory(contextDomain, constants.strings.PrimarySource); });
            document.getElementsByClassName("context_newsSite")[0].addEventListener('click', () => { changeSiteCategory(contextDomain, constants.strings.NewsSite); });
            document.getElementsByClassName("context_socialMedia")[0].addEventListener('click', () => { changeSiteCategory(contextDomain, constants.strings.SocialMedia); });
            document.getElementsByClassName("context_misc")[0].addEventListener('click', () => { changeSiteCategory(contextDomain, constants.strings.Misc); });
        }
        document.getElementsByClassName("contextTitle")[0].addEventListener('click', closeContextMenus);
        document.getElementsByClassName("context_hideDomain")[0].addEventListener('click', () => { hideSite(legendItem); });
    });
}

function closeContextMenus() {
    document.getElementById('contextMenus').innerHTML = "";
}

function changeSiteCategory(hostname, newCategory) {
    UserSettings.fetch().then(settings => {
        if (settings.changeSiteCategory(hostname, newCategory))
        {
            chartData.refreshCategories(settings);
            drawLinkCategoriesChart();
            drawLinkDetailsChart();

            if (settings.sendSiteCategories)
            {
                var siteMapping = new SiteCategoryMapping(hostname, newCategory);
                uploadUserSiteCategory(siteMapping, settings);
            }
        }
    });

    closeContextMenus();
}

function uploadUserSiteCategory(siteMapping, settings) {
    if (!siteMapping || settings.sendSiteCategories == false)
    {
        utils.log("Not Uploading Site Category")();
        return;
    }

    const request = new Request(constants.urls.pssIngestAPI, {
        method: 'POST',
        body: JSON.stringify({ siteCategoryData: siteMapping }),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    });

    fetch(request)
        .then(res => { utils.log("Uploaded Site Category.")(siteMapping); });
}

function hideSite(legendItem) {
    chartData.removeSite(legendItem.text);
    UserSettings.fetch().then(settings => {
        chartData.refreshCategories(settings);
        drawLinkDetailsChart();
        drawLinkCategoriesChart();
    });
    closeContextMenus();
}
/* #endregion - UI Updates **/

/* #region - PostMessages **/

function rescanPage() {
    currentFilterCategories = [...defaultFilterCategories];
    UserSettings.fetch(true);
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.RequestLinkAnalysis
        });
    });
}

function scanUnsupportedSite() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.ScanUnsupportedSite
        });
    });
}

function customDivSelection()
{
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.CustomDivSelection
        });
    });
}

function handlePostMessages(request) {
    if (!request)
    {
        utils.getCurrentHostName().then((articleHostname) => showErrorMessage(articleHostname));
    }
    else if (request.message == constants.postMessages.SiteNotSupported || request.message == constants.postMessages.SiteIsInIgnoreList)
    {
        showErrorMessage(request.hostname);
    } else if (request.message == constants.postMessages.ArticleNotFound)
    {
        showArticleNotFoundMessage(request.isHomePage);
    } else if (request.message == constants.postMessages.LinkAnalysisResult)
    {
        drawCharts(request.result, request.unsupported);
    } else if (request.message == constants.postMessages.NewArticleFound)
    {
        newArticle = true;
        showNewArticleMessage();
    }
}

/* #endregion - PostMessages **/

/** SCRIPT LOAD **/
document.addEventListener('DOMContentLoaded', function () {

    initPopupUI();

    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            handlePostMessages(request);
        }
    );

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            message: constants.postMessages.RequestLinkAnalysis
        });
    });

    var button = document.getElementById('leftButton');
    button.onclick = rescanPage;
}, false);

UserSettings.fetch();
