/** utils.js
 * 
 * Various utility functions
 */

function getHostNameFromUrl(url) {
    var hostName = url.hostname;
    if (hostName.startsWith('www.'))
    {
        hostName = hostName.substr(4);
    }

    return constants.aka[hostName] == undefined ? hostName : constants.aka[hostName];
}

function cleanUrl(url) {
    var hostName = url.hostname;
    var hadWWW = false;
    if (hostName.startsWith('www.'))
    {
        hostName = hostName.substr(4);
        hadWWW = true;
    }
    url.hostname = (hadWWW ? "www." : "") + (constants.aka[hostName] == undefined ? hostName : constants.aka[hostName]);
    var result = url.href;
    if(result.endsWith('/'))
    {
        result = result.substr(0, result.length - 1);
    }
    if(result.endsWith('?amp'))
    {
        result = result.substr(0, result.length - 4); 
    }
    return result;
}

function isSubdomain(url, domain) {
    return url.endsWith(domain);
}

function getCurrentHostName() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            resolve(getHostNameFromUrl(new URL(tabs[0].url)));
        });
    });
}

function deDupLinkArray(externalLinks) {
    var uniqueLinks = externalLinks.reduce((acc, val) => {
        url = cleanUrl(new URL(val.url));
        acc[url] = acc[url] == undefined ? 1 : acc[url] + 1;
        return acc;
    }, {});
    return uniqueLinks
}

function sortObject(obj) { return Object.keys(obj).sort().reduce((res, key) => (res[key] = obj[key], res), {}) }

var debugLogs = true;
function log(...args) {
    return (data) => {
        if (debugLogs == true)
        {
            args.unshift("PSS: ");
            if (data)
            {
                console.log.apply(null, args.concat([data]));
            }
            else
            {
                console.log.apply(null, args);
            }
        }
        return data;
    }
}

function warn(...args) {
    return (data) => {
        args.unshift("PSS: ");
        if (data)
        {
            console.warn.apply(null, args.concat([data]));
        }
        else
        {
            console.warn.apply(null, args);
        }
        return data;
    }
}


function rgbToHex(color) {
    color = "" + color;
    if (!color || color.indexOf("rgb") < 0)
    {
        return;
    }

    if (color.charAt(0) == "#")
    {
        return color;
    }

    var components = /(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(color),
        r = parseInt(components[2], 10).toString(16),
        g = parseInt(components[3], 10).toString(16),
        b = parseInt(components[4], 10).toString(16);

    return "#" + (
        (r.length == 1 ? "0" + r : r) +
        (g.length == 1 ? "0" + g : g) +
        (b.length == 1 ? "0" + b : b)
    );
}

// https://stackoverflow.com/questions/35969656/how-can-i-generate-the-opposite-color-according-to-current-color
function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0)
    {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3)
    {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6)
    {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);

    // http://stackoverflow.com/a/3943023/112731
    return (r * 0.299 + g * 0.587 + b * 0.114) > 120
        ? '#000000'
        : '#FFFFFF';
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function deleteCookie(cname)
  {
      setCookie(cname, '', -10);
  }


var utils = {
    cleanUrl,
    deDupLinkArray,
    deleteCookie,
    getCookie,
    getCurrentHostName,
    getHostNameFromUrl,
    invertColor,
    isSubdomain,
    log,
    rgbToHex,
    setCookie,
    sortObject,
    warn
};