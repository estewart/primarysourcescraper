/** UserSettings.js
 * 
 * This file handles all user settings in the browser's sync storage.
 * 
 * This manages a singleton UserSettings object that can be obtained via the static fetch() call.
 */

class UserSettings {

    constructor(
        highlighterColor = constants.colors.HighlighterDefault,
        autoHighlight = true,
        optimisticParsing = false,
        onlyHighlightPrimarySources = false,
        sendArticleData = false,
        sendSiteCategories = false,
        sendErrorReports = false,
        newArticleCount = 0,
        userIgnoredDomains = { add: [], remove: [] },
        userCategorizedSites = {},
        lastPrivacyUpdateVersion = "") {
        this.highlighterColor = highlighterColor;
        this.autoHighlight = autoHighlight;
        this.optimisticParsing = optimisticParsing;
        this.onlyHighlightPrimarySources = onlyHighlightPrimarySources;
        this.sendArticleData = sendArticleData;
        this.sendSiteCategories = sendSiteCategories;
        this.sendErrorReports = sendErrorReports;
        this.newArticleCount = newArticleCount;
        this.userIgnoredDomains = userIgnoredDomains;
        this.userCategorizedSites = userCategorizedSites;
        this.lastPrivacyUpdateVersion = lastPrivacyUpdateVersion;

        this.ignoredDomains = [];
        this.linkCategories = {};
    }

    incrementNewArticleCount() {
        this.newArticleCount++;
        this.commit();
    }

    isIgnoredDomain(url) {
        var ignoredDomain = false;
        this.ignoredDomains.forEach(domain => {
            if (url.includes(domain))
            {
                ignoredDomain = true;
            }
        });
        return ignoredDomain;
    }

    addIgnoredUrl(url) {
        if (url && url != "")
        {
            if (!this.ignoredDomains.includes(url))
            {
                if(this.userIgnoredDomains.remove.includes(url))
                {
                    this.userIgnoredDomains.remove.splice(this.userIgnoredDomains.remove.indexOf(url));
                }else{
                    this.userIgnoredDomains.add.push(url);
                }
                
                this.ignoredDomains.push(url);
                this.commit();
            }
        }
    }

    removeIgnoredUrl(url) {
        if (url && url != "")
        {
            if (this.ignoredDomains.includes(url))
            {
                if(this.userIgnoredDomains.add.includes(url))
                {
                    this.userIgnoredDomains.add.splice(this.userIgnoredDomains.add.indexOf(url));
                }else{
                    this.userIgnoredDomains.remove.push(url);
                }               
                this.ignoredDomains.splice(this.ignoredDomains.indexOf(url), 1)
                this.commit();
            }
        }
    }

    clearAllIgnoredUrls() {
        this.userIgnoredDomains.remove = this.userIgnoredDomains.remove.concat(this.ignoredDomains);
        this.ignoredDomains = [];
        this.commit();
    }

    loadDefaultIgnoredUrls() {
        return new Promise((resolve, reject) => {
            var settings = this;
            fetch(chrome.extension.getURL('./settings/defaultIgnoredDomains.json'))
                .then((resp) => resp.json())
                .then(function (jsonData) {
                    settings.ignoredDomains = jsonData;
                    settings.commit();
                    resolve(settings.ignoredDomains);
                });
        });
    }

    resetIgnoredUrlsToDefault() {
        this.userIgnoredDomains = { add: [], remove: [] };
        return this.loadDefaultIgnoredUrls();
    }

    getSiteCategory(hostname) {
        var result = constants.strings.PrimarySource;
        var settings = this;
        if(this.linkCategories[hostname])
        {
            result = this.linkCategories[hostname];
        }
        else{
            Object.keys(settings.linkCategories).forEach(url => {
                if(utils.isSubdomain(hostname, url))
                {
                    result = settings.linkCategories[url];
                }
            });
        }
        return result;
    }

    isUserDefinedSiteCategory(hostname)
    {
        return this.userCategorizedSites[hostname] != undefined;
    }

    changeSiteCategory(hostname, newCategory) {
        var oldCategory = this.getSiteCategory(hostname);
        if (oldCategory == newCategory)
        {
            return false;
        }

        this.userCategorizedSites[hostname] = newCategory;
        this.mergeUserCategorizedSites();
        this.commit();
        return true;
    }

    removeUserSiteCategory(hostname)
    {
        delete this.userCategorizedSites[hostname];
        this.commit();
    }

    loadDefaultLinkCategories() {
        return new Promise((resolve, reject) => {
            var settings = this;
            fetch(chrome.extension.getURL('./settings/linkCategories.json'))
                .then((resp) => resp.json())
                .then(function (jsonData) {
                    settings.linkCategories = {};
                    Object.keys(jsonData).forEach(category => {
                        jsonData[category].forEach(url => {
                            settings.linkCategories[url] = category;
                        });
                    });
                    settings.commit();
                    resolve(settings.linkCategories);
                });
        });
    }

    resetLinkCategoriesToDefault() {
        this.userCategorizedSites = {};
        return this.loadDefaultLinkCategories();
    }

    mergeUserIgnoredDomains() {
        var settings = this;
        settings.userIgnoredDomains.add.forEach(siteToAdd => {
            if (!settings.ignoredDomains.includes(siteToAdd))
            {
                settings.ignoredDomains.push(siteToAdd);
            }
        });

        settings.userIgnoredDomains.remove.forEach(siteToRemove => {
            var index = settings.ignoredDomains.indexOf(siteToRemove);
            if (index >= 0)
            {
                settings.ignoredDomains.splice(index, 1);
            }
        });
    }

    mergeUserCategorizedSites() {
        var settings = this;
        Object.keys(settings.userCategorizedSites).forEach(url => {
            settings.linkCategories[url] = settings.userCategorizedSites[url]
        });
    }

    getSortedUserCategories()
    {
        return utils.sortObject(this.userCategorizedSites);
    }

    static fetch(invalidate = false) {
        if (!invalidate && UserSettings.settings)
        {
            return Promise.resolve(UserSettings.settings);
        } else if (!invalidate && UserSettings.settingsPromise)
        {
            return UserSettings.settingsPromise;
        } else
        {
            UserSettings.settingsPromise = new Promise((resolve, reject) => {
                chrome.storage.sync.get({
                    settings: new UserSettings()
                }, function (items) {
                    // Even though we just read a UserSettings object, it doesn't have the right UserSettings prototype set up.
                    // There's probably a better way to set this up, but this copy operation works for now.
                    UserSettings.settings = new UserSettings(
                        items.settings.highlighterColor,
                        items.settings.autoHighlight,
                        items.settings.optimisticParsing,
                        items.settings.onlyHighlightPrimarySources,
                        items.settings.sendArticleData,
                        items.settings.sendSiteCategories,
                        items.settings.sendErrorReports,
                        items.settings.newArticleCount,
                        items.settings.userIgnoredDomains,
                        items.settings.userCategorizedSites,
                        items.settings.lastPrivacyUpdateVersion);

                    Promise.all([UserSettings.settings.loadDefaultLinkCategories(), UserSettings.settings.loadDefaultIgnoredUrls()])
                        .then(values => {
                            UserSettings.settings.mergeUserIgnoredDomains();
                            UserSettings.settings.mergeUserCategorizedSites();
                            resolve(UserSettings.settings);
                        });
                });
            });
            return UserSettings.settingsPromise;
        }
    }

    commit() {
        chrome.storage.sync.set({
            settings: this
        });
    }
}