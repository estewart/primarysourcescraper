/** ArticleData.js
 * 
 * This file is locked via repo settings.  Any changes to this file MUST be accompanied by changes to the
 * PSS Privacy Policy and Data Collection pages in the pages branch.
 * 
 * Any updates to this file should also bump the lastPrivacyUpdateVersion in constants.js to trigger UI 
 * indicating the privacy policy has been updated.
 * 
 * These classes contain all of the article properties that PSS can collect via our data ingestion API.
 * 
 * These classes MUST NOT contain anything that is likely to contain personal data from users.
 */

class ArticleData {
    constructor(title, date, location, totalLinkCount, externalLinks) {
        this.article = {
            title: title,
            date: date,
            location: location
        };
        this.totalLinkCount = totalLinkCount;
        this.externalLinks = externalLinks;

        Object.freeze(this);
    }
}

class ExternalLink {
    constructor(url, text) {
        this.url = url;
        this.text = text;

        Object.freeze(this);
    }
}

class SiteCategoryMapping {
    constructor(hostname, category)
    {
        this.hostname = hostname;
        this.category = category;

        Object.freeze(this);
    }
}

class ErrorReport {
    constructor(errorType, errorInfo, location, version)
    {
        this.errorType = errorType;
        this.errorInfo = errorInfo;
        this.location = location;   
        this.version = version;

        Object.freeze(this);
    }
}
