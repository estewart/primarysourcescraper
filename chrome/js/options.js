/** options.js
 * 
 * This file acts on options.html to display the current UserSettings in a readable format and allow the user to manage their settings.
 */

/* #region - Filter List Handler **/

function showRelevantDomains(searchType) {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById(searchType + "Search");
    filter = input.value.toUpperCase();
    ul = document.getElementById(searchType + "List");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++)
    {
        a = li[i]; //.getElementsByTagName("text")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1)
        {
            li[i].style.display = "";
        } else
        {
            li[i].style.display = "none";
        }
    }
}

/* #endregion - Filter List Handler **/

/* #region - Write User Settings **/

function initOptionsHandlers(settings) {
    var colorPicker = document.getElementById('color');
    colorPicker.addEventListener('change', (event) => {
        settings.highlighterColor = event.target.value;
        settings.commit();
    });

    var autoHighlight = document.getElementById('autoHighlight');
    autoHighlight.addEventListener('change', (event) => {
        settings.autoHighlight = autoHighlight.checked;
        settings.commit();
    });

    var onlyHighlightPrimarySources = document.getElementById('onlyHighlightPrimarySources');
    onlyHighlightPrimarySources.addEventListener('change', (event) => {
        settings.onlyHighlightPrimarySources = onlyHighlightPrimarySources.checked;
        settings.commit();
    });

    var optimisticParsing = document.getElementById('optimisticParsing');
    optimisticParsing.addEventListener('change', (event) => {
        settings.optimisticParsing = optimisticParsing.checked;
        settings.commit();
    });

    var sendArticleData = document.getElementById('sendArticleData');
    sendArticleData.addEventListener('change', (event) => {
        settings.sendArticleData = sendArticleData.checked;
        settings.commit();
        updateArticleSharingStatus(settings);
    });

    var sendSiteCategories = document.getElementById('sendSiteCategories');
    sendSiteCategories.addEventListener('change', (event) => {
        settings.sendSiteCategories = sendSiteCategories.checked;
        settings.commit();
        updateCategorySharingStatus(settings);
    });

    var sendErrorReports = document.getElementById('sendErrorReports');
    sendErrorReports.addEventListener('change', (event) => {
        settings.sendErrorReports = sendErrorReports.checked;
        settings.commit();
    });

    var color_picker_wrapper = document.getElementById("color-picker-wrapper");
    colorPicker.onchange = function () {
        color_picker_wrapper.style.backgroundColor = colorPicker.value;
    }
    color_picker_wrapper.style.backgroundColor = colorPicker.value;
}

function addIgnoredUrl() {
    var newDomain = document.getElementById("ignoredDomainsSearch").value;
    if (newDomain && newDomain != "")
    {
        UserSettings.fetch()
            .then(settings => settings.addIgnoredUrl(newDomain))
            .then(loadIgnoredSites);
    }
}

function removeIgnoredUrl() {
    var domainToRemove = document.getElementById("ignoredDomainsSearch").value;
    if (domainToRemove && domainToRemove != "")
    {
        UserSettings.fetch()
            .then(settings => settings.removeIgnoredUrl(domainToRemove))
            .then(loadIgnoredSites);
    }
}

function clearAll() {
    UserSettings.fetch()
        .then(settings => settings.clearAllIgnoredUrls())
        .then(loadIgnoredSites);
}

function resetIgnoreListToDefault() {
    UserSettings.fetch()
        .then(settings => settings.resetIgnoredUrlsToDefault())
        .then(loadIgnoredSites);
}

function resetSiteCategoriesToDefault()
{
    UserSettings.fetch()
    .then(settings => settings.resetLinkCategoriesToDefault())
    .then(loadUserCategorizedSites);
}

/* #endregion - Write User Settings **/

/* #region - Read User Settings **/

function restoreOptions(settings) {

    if(settings.lastPrivacyUpdateVersion < constants.versions.lastPrivacyUpdateVersion)
    {
        firstRunAnimation();
        settings.lastPrivacyUpdateVersion = constants.versions.current;
        settings.commit();
    }
    document.getElementById("newArticleCount").value = settings.newArticleCount;
    document.getElementById("sendArticleData").checked = settings.sendArticleData;
    document.getElementById("sendSiteCategories").checked = settings.sendSiteCategories;
    document.getElementById("sendErrorReports").checked = settings.sendErrorReports;
    document.getElementById("autoHighlight").checked = settings.autoHighlight;
    document.getElementById("onlyHighlightPrimarySources").checked = settings.onlyHighlightPrimarySources;
    document.getElementById("optimisticParsing").checked = settings.optimisticParsing;
    document.getElementById("color").value = settings.highlighterColor;

    loadIgnoredSites();
    loadSupportedSites();
    loadUserCategorizedSites();
    updateArticleSharingStatus(settings);
}

function loadIgnoredSites() {
    document.getElementById("ignoredDomainsSearch").value = "";
    showRelevantDomains("ignoredDomains");
    // Restore the ignored domains list
    UserSettings.fetch().then(settings => {
        var ignoredDomainsList = document.getElementById('ignoredDomainsList');
        while (ignoredDomainsList.firstChild)
        {
            ignoredDomainsList.removeChild(ignoredDomainsList.firstChild);
        }
        if (settings.ignoredDomains.length == 0)
        {
            var li = document.createElement("li");
            var textNode = document.createTextNode("No Ignored Domains");
            li.appendChild(textNode);
            ignoredDomainsList.appendChild(li)
        } else
        {
            settings.ignoredDomains.sort().forEach(element => {
                var li = document.createElement("li");
                var textNode = document.createTextNode(element);
                li.addEventListener('click', () => { document.getElementById("ignoredDomainsSearch").value = element });
                var closeButton = document.createElement("i");
                closeButton.textContent = "close";
                closeButton.style.float = "right";
                closeButton.classList.add("material-icons");
                closeButton.addEventListener("click", () => {settings.removeIgnoredUrl(element); loadIgnoredSites()});
                li.appendChild(textNode);
                li.appendChild(closeButton);
                ignoredDomainsList.appendChild(li)
            });
        }
        document.getElementById("ignoredDomainCount").value = settings.ignoredDomains.length;
    });
}

function loadUserCategorizedSites() {
    // Restore the ignored domains list
    UserSettings.fetch().then(settings => {
        var userCategorizedDomainsList = document.getElementById('userCategorizedDomainsList');
        while (userCategorizedDomainsList.firstChild)
        {
            userCategorizedDomainsList.removeChild(userCategorizedDomainsList.firstChild);
        }

        var categorizedCount = 0;

        Object.keys(settings.getSortedUserCategories()).forEach(url => {
                var li = document.createElement("li");
                var textNode = document.createTextNode(url);
                var categoryPill = document.createElement("a");
                categoryPill.textContent = settings.userCategorizedSites[url];
                categoryPill.classList.add("categoryPill", getClassForSiteCategory(settings.userCategorizedSites[url]));
                var closeButton = document.createElement("i");
                closeButton.textContent = "close";
                closeButton.style.float = "right";
                closeButton.classList.add("material-icons");
                closeButton.addEventListener("click", () => {settings.removeUserSiteCategory(url); loadUserCategorizedSites()});
                li.appendChild(textNode);
                li.appendChild(closeButton);
                li.appendChild(categoryPill);
                userCategorizedDomainsList.appendChild(li);
                categorizedCount++;
        });
        document.getElementById("userCategorizedSitesCount").value = categorizedCount;
        updateCategorySharingStatus(settings);
    });
}

function updateCategorySharingStatus(settings)
{
    document.getElementById("sharingCategoriesOutput").value = settings.sendSiteCategories ? "sharing your categories. Thank you!" : "not sharing your categories.";
}

function updateArticleSharingStatus(settings)
{
    document.getElementById("sharingArticleDataOutput").value = settings.sendArticleData ? "uploading article data. Thank you!" : "not uploading article data.";
}

function getClassForSiteCategory(category)
{
    var result = "";
    switch(category)
    {
        case constants.strings.PrimarySource:
            result = "primarySource";
        break;
        case constants.strings.NewsSite:
            result = "newsSite";
            break;
        case constants.strings.SocialMedia:
            result = "socialMedia";
            break;
        case constants.strings.Misc:
            result = "misc";
            break;
    }
    return result;
}

/* #endregion - Read User Settings **/

/* #region - Read App Settings **/

function loadSupportedSites() {

    document.getElementById("supportedDomainsSearch").value = "";
    showRelevantDomains("supportedDomains");
    // Restore the supported domains list
    fetch(chrome.extension.getURL('./settings/newsSources.json'))
        .then((resp) => resp.json())
        .then(function (jsonData) {
            document.getElementById("supportedDomainCount").value = Object.keys(jsonData).length;
            var supportedDomainsList = document.getElementById('supportedDomainsList');
            Object.keys(jsonData).forEach(key => {
                var li = document.createElement("li");
                var textNode = document.createTextNode(key);
                li.addEventListener('click', () => { document.getElementById("supportedDomainsSearch").value = key });
                li.appendChild(textNode);
                supportedDomainsList.appendChild(li)
            });
        });
}
/* #endregion - Read App Settings **/

/* #region - Initialize Page **/

function onPageLoaded() {
    document.getElementById('add').addEventListener('click', addIgnoredUrl);
    document.getElementById('remove').addEventListener('click', removeIgnoredUrl);
    document.getElementById('removeAll').addEventListener('click', clearAll);
    document.getElementById('resetToDefault').addEventListener('click', resetIgnoreListToDefault);
    document.getElementById('resetCategories').addEventListener('click', resetSiteCategoriesToDefault);
    document.getElementById('ignoredDomainsSearch').addEventListener('keyup', () => { showRelevantDomains("ignoredDomains"); });
    document.getElementById('supportedDomainsSearch').addEventListener('keyup', () => { showRelevantDomains("supportedDomains"); });
    document.getElementById('userCategorizedDomainsSearch').addEventListener('keyup', () => { showRelevantDomains("userCategorizedDomains"); });

    document.getElementById('supportedDomainsTabButton').addEventListener('click', (event) => { switchToTab('supportedDomains'); });
    document.getElementById('ignoredDomainsTabButton').addEventListener('click', (event) => { switchToTab('ignoredDomains'); });
    document.getElementById('siteCategoriesTabButton').addEventListener('click', (event) => { switchToTab('siteCategories'); });

    UserSettings.fetch().then(settings => {
        restoreOptions(settings);
        initOptionsHandlers(settings);
    });

    var selectedTab = utils.getCookie('selectedTab');
    if(selectedTab != "") {
        switchToTab(selectedTab);
    }else 
    {
        switchToTab('supportedDomains');
    }

}

/* #endregion - Initialize Page **/

/* #region - UI Handlers **/
function switchToTab(tabName) {
    // Set cookie so we refresh to the same tab
    utils.setCookie('selectedTab', tabName, 1);

    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++)
    {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tabLink");
    for (i = 0; i < tablinks.length; i++)
    {
        tablinks[i].className = tablinks[i].className.replace(" inactive", "");
        tablinks[i].className += " inactive";
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    document.getElementById(tabName + "TabButton").className = document.getElementById(tabName + "TabButton").className.replace(" inactive", "");
}

function firstRunAnimation()
{
    var privacyHeader = document.getElementById('privacyHeader');
    privacyHeader.classList.add("blink");
}
/* #endregion - UI Handlers **/

/** SCRIPT LOAD **/
document.addEventListener('DOMContentLoaded', onPageLoaded);