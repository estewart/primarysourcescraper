/** badge.js
 * 
 * This file manages the badge text and color on the PSS icon.
 */
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        var badgeText = "";
        var badgeColor = "";

        if (request.message == constants.postMessages.LinkAnalysisResult)
        {
            var uniqueLinks = utils.deDupLinkArray(request.result.externalLinks);
            badgeText += Object.keys(uniqueLinks).length;
            badgeColor = constants.colors.BadgeDefault;
        } else if (request.message == constants.postMessages.NewArticleFound)
        {
            badgeText += request.text;
            badgeColor = constants.colors.BadgeNewArticle;
        } else if (request.message == constants.postMessages.SiteNotSupported)
        {
            badgeText = "??";
            badgeColor = constants.colors.BadgeUnsupported;
        }
        chrome.browserAction.setBadgeText({ text: badgeText, tabId: sender.tab.id })
        if (request.message != constants.postMessages.SiteIsInIgnoreList && request.message != constants.postMessages.ArticleNotFound)
        {
            chrome.browserAction.setBadgeBackgroundColor({ color: badgeColor, tabId: sender.tab.id });
        }
    }
);