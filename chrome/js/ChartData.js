/** ChartData.js
 * 
 * This file handles the data model for the charts in the popup.
 */

class ChartData {

    constructor(articleHostname, articleData, settings)
    {
        var labels = [];
        var uniqueCount = [];
        var totalCount = [];

        this.articleHostname = articleHostname;
        var externalLinks = utils.deDupLinkArray(articleData.externalLinks);
        this.externalLinkCount = Object.keys(externalLinks).length;
        this.internalLinkCount = (articleData.totalLinkCount - this.externalLinkCount);

        this.ratioLabels = [constants.strings.Internal, constants.strings.SocialMedia, constants.strings.NewsSite, constants.strings.PrimarySource, constants.strings.Misc];
        this.ratioData = [this.internalLinkCount, 0, 0, 0, 0];
        Object.keys(externalLinks).forEach(link => {
            var hostname = utils.getHostNameFromUrl(new URL(link));

            var typeIndex = this.ratioLabels.indexOf(settings.getSiteCategory(hostname));
            this.ratioData[typeIndex]++;

            var index = labels.indexOf(hostname);
            if (index == -1)
            {
                labels.push(hostname);
                uniqueCount.push(1);
                totalCount.push(externalLinks[link]);
            } else
            {
                uniqueCount[index]++;
                totalCount[index] += externalLinks[link];
            }
        });
        
        this.sortChartData(labels, uniqueCount, totalCount);
    }

    sortChartData(labels, uniqueCount, totalCount) {
        // Sort the data
        var chartData = labels.map(function (label, index) {
            return {
                label: label,
                data: uniqueCount[index] || 0,
                totalCount: totalCount[index] || 0
            };
        });
    
        var sortedChartData = chartData.sort(function (a, b) {
            if (a.data == b.data)
            {
                return a.label.localeCompare(b.label);
            }
            return b.data - a.data;
        });
    
        this.detailLabels = [];
        this.detailData = [];
        this.detailMetadata = [];
        sortedChartData.forEach(entry => {
            this.detailLabels.push(entry.label);
            this.detailData.push(entry.data);
            this.detailMetadata.push(entry.totalCount);
        });
    }

    refreshCategories(settings)
    {
        this.ratioData = [this.internalLinkCount, 0, 0, 0, 0];
        var detailIndex = 0;
        this.detailLabels.forEach(hostname => {
            var typeIndex = this.ratioLabels.indexOf(settings.getSiteCategory(hostname));
            this.ratioData[typeIndex] += this.detailData[detailIndex];
            detailIndex++;
        });
    }

    removeSite(hostname)
    {
        var index = this.detailLabels.indexOf(hostname);
        if(index > -1)
        {
            this.detailLabels.splice(index, 1);
            this.detailData.splice(index, 1);
            this.detailMetadata.splice(index, 1);
        }
    }

}