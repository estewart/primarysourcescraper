#!/bin/bash

pushd /d/Programming/PSS/primarySourceScraper/chrome > /dev/null
CURRENT_BRANCH=$(git branch --show-current);
#echo "Current branch is: $CURRENT_BRANCH";

CURRENT_VERSION=$(grep -oP 'version": \"\K.*(?=")' ./manifest.json | head -1);
#echo "Current version is: $CURRENT_VERSION";

VERSION_ARRAY=($(echo $CURRENT_VERSION | tr "." "\n"));
MINOR_COMPONENT=$(echo ${VERSION_ARRAY[2]});
#echo "Minor Version is: $MINOR_COMPONENT";

MINOR_COMPONENT=$(expr $MINOR_COMPONENT + 1);
VERSION_ARRAY[2]=$MINOR_COMPONENT;
#echo "New Minor Version is: ${VERSION_ARRAY[2]}";

NEW_VERSION="";
for i in "${VERSION_ARRAY[@]}"
do
    NEW_VERSION="$NEW_VERSION.$i";
done
NEW_VERSION=${NEW_VERSION:1};

#echo "New version is: $NEW_VERSION";
grep -rl $CURRENT_VERSION | $(xargs sed -i s/$CURRENT_VERSION/$NEW_VERSION/g);

git add ./** &> /dev/null

#git commit -m "Bump Version to $NEW_VERSION" > /dev/null 2>&1

popd > /dev/null