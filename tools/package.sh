#!/bin/bash

# Change to Extension directory
pushd /d/Programming/PSS/primarySourceScraper/chrome > /dev/null

# Disable debug logging
sed -i 's/var debugLogs = true;/var debugLogs = false;/g' ./js/utils.js

zip -qr pss . 
cp pss.zip /C/Users/enige/OneDrive/PrimarySourceScraper/pss.zip
mv pss.zip ../

git add ../pss.zip;

# Enable debug logging
sed -i 's/var debugLogs = false;/var debugLogs = true;/g' ./js/utils.js

#git commit -m "Bump Version to $NEW_VERSION" > /dev/null 2>&1
popd > /dev/null